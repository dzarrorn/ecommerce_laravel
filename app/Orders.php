<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function pembayaran()
    {
        return $this->hasMany('App\Pembayaran', 'pembayaran_id', 'id');
    }

    public function product()
    {
        return $this->hasMany('App\Product', 'product_id', 'id');
    }
}

