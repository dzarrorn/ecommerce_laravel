<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    public function user()
    {
        return $this->hasMany('App\User', 'user_id', 'id');
    }

    public function product()
    {
        return $this->hasMany('App\Product', 'product_id', 'id');
    }
}
