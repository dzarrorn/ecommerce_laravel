<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alamat extends Model
{
    protected $table = 'alamat';
    protected $fillable = ['alamat','kota/kab','kecamatan','kode_pos'];
    public $timestamps = false;
    public function phone()
    {
        return $this->hasOne('App\Phone');
    }
    public function profil()
    {
        return $this->hasOne('App\Profil');
    }
}
