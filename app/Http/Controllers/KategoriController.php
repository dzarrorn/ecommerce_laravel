<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;

class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);
        $query = DB::table('kategori')->insert([
            "nama" => $request["nama"],
        ]);
        Alert::success('Success', 'Success Add Category');
        return redirect('/kategori');
    }

    public function index()
    {
        $kategori = DB::table('kategori')->get();
        return view('kategori.index', compact('kategori'));
    }

    public function show($id)
    {
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view('kategori.show', compact('kategori'));
    }

    public function edit($id)
    {
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view('kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $query = DB::table('kategori')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
            ]);
        Alert::success('Success', 'Success Edit Category');
        return redirect('/kategori');
    }

    public function destroy($id)
    {
        $query = DB::table('kategori')->where('id', $id)->delete();
        Alert::warning('Delete', 'Delete Category');
        return redirect('/kategori');
    }
}
