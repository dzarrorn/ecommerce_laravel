<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Profil;
use App\Alamat;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'tgl_lahir' => ['required', 'date'],
            'tmpt_lahir' => ['required', 'string'],
            'alamat' => ['required', 'string'],
            'kota/kab' => ['required', 'string'],
            'kecamatan' => ['required', 'string'],
            'kode_pos' => ['required', 'string']

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            
        ]);
        $alamat = Alamat::create([
            'alamat' => $data['alamat'],
            'kota/kab' => $data['kota/kab'],
            'kecamatan' => $data['kecamatan'],
            'kode_pos' => $data['kode_pos']
        ]);
        Profil::create([
            'tgl_lahir'=> $data['tgl_lahir'],
            'tmpt_lahir'=> $data['tmpt_lahir'],
            'user_id'=> $user->id,
            'alamat_id'=>$alamat->id
        ]);
        return $user;
        return $alamat;
        
        
    }
}
