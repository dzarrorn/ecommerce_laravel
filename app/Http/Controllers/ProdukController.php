<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use App\Kategori;
use App\Product;
use File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   


    public function index()
    {
        $product = Product::all();
        return view('product.index',compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('product.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama'=>'required',
            'harga'=>'required',
            'gambar'=>'mimes:jpeg,jpg,png|max:2200',
            'deskripsi'=>'required',
            'stok'=>'required',
            'kategori_id'=>'required',
        ]);
        $gambar = $request->gambar;
        $new_gambar = time(). ' - ' . $gambar->getClientOriginalName();
        $product = Product::create([
            'nama'=>$request->nama,
            'harga'=>$request->harga,
            'gambar'=>$new_gambar,
            'deskripsi'=>$request->deskripsi,
            'stok'=>$request->stok,
            'kategori_id'=>$request->kategori_id,
        ]);

        $gambar->move('gambar/',$new_gambar);
        Alert::success('Success', 'Success Add Product');

        return redirect('product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('product.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $kategori = Kategori::all();
        return view('product.edit',compact('product','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama'=>'required',
            'harga'=>'required',
            'gambar'=>'mimes:jpeg,jpg,png|max:2200',
            'deskripsi'=>'required',
            'stok'=>'required',
            'kategori_id'=>'required',
        ]);
        $product=Product::findorfail($id);
        if ($request->has('gambar')) {
            File::delete('gambar/'.$product->gambar);
            $gambar = $request->gambar;
            $new_gambar = time(). ' - ' . $gambar->getClientOriginalName();
            $gambar->move('gambar/',$new_gambar);
            $product_data = [
            'nama'=>$request->nama,
            'harga'=>$request->harga,
            'gambar'=>$new_gambar,
            'deskripsi'=>$request->deskripsi,
            'stok'=>$request->stok,
            'kategori_id'=>$request->kategori_id,
            ];
        }else {
            $product_data = [
            'nama'=>$request->nama,
            'harga'=>$request->harga,
            'deskripsi'=>$request->deskripsi,
            'stok'=>$request->stok,
            'kategori_id'=>$request->kategori_id,
            ];
        }
        $product->update($product_data);
        Alert::success('Success', 'Success Edit Product');
        return redirect('/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::findorfail($id);
        $product->delete();
        $path='gambar/';
        File::delete($path.$product->gambar);
        Alert::warning('Delete', 'Delete Product');
        return redirect('/product'); 
    }
}
