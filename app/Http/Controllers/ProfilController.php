<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profil;
use App\Alamat;
use App\User;
use Auth;
class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profil=Profil::where('user_id',Auth::user()->id)->first();
        return view('profile.index',compact('profil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tgl_lahir'=>'required',
            'tmpt_lahir'=>'required',
            'alamat'=>'required',
            'kota/kab'=>'required',
            'kecamatan'=>'required',
            'kode_pos'=>'required',
            'name'=>'required',
        ]);
        $profil_data=[
            'tgl_lahir'=> $request->tgl_lahir,
            'tmpt_lahir'=> $request->tmpt_lahir,
        ];
        $kota='kota/kab';
        $alamat_data=[
            'alamat'=> $request->alamat,
            'kota/kab'=> $request->$kota,
            'kecamatan'=> $request->kecamatan,
            'kode_pos'=> $request->kode_pos,
        ];
        Profil::whereId($id)->update($profil_data);
        Alamat::whereId($id)->update($alamat_data);
        return redirect('/account');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
