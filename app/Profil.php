<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profil';
    protected $fillable = ['tgl_lahir','tmpt_lahir','user_id','alamat_id'];
    public $timestamps = false;
    public function alamat(){
        return $this->belongsTo('App\Alamat');
    }
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }


    // public function user()
    // {
    //     return $this->belongsTo('App\User', 'user_id', 'id');
    // }

    // public function alamat()
    // {
    //     return $this->belongsTo('App\Alamat', 'alamat_id', 'id');
    // }
 
}
