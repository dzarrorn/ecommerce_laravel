<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['nama'];
    public $timestamps = false;
    public function kategori()
    {
        return $this->hasOne('App\Product');
    }
}
