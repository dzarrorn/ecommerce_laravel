<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'produk';
    protected $fillable = ['nama','deskripsi','harga','stok','gambar','kategori_id']; 
    public $timestamps = false;

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'kategori_id', 'id');
    }
}
