<h1>
    Kelompok 4
    <h1>
        Anggota Kelompok
        <ul>
            <li>
                <p>
                    Anwar Kholidi Nasution (@anwarkholidinasution)
                </p>
            </li>
            <li>
                <p>
                    Dliya' Zarror Nibros (@dzarrorn)
                </p>
            </li>
            <li>
                <p>
                    Siti Indriyana (@Indrynaa)
                </p>
            </li>
        </ul>
    </h1>
</h1>
<h5>
    link heroku:
    <a href="https://furniture4.herokuapp.com/"
        >https://furniture4.herokuapp.com/</a
    >
</h5>
<h5>
    link github:
    <a href="https://gitlab.com/dzarrorn/ecommerce_laravel"
        >https://gitlab.com/dzarrorn/ecommerce_laravel</a
    >
</h5>
<img src="public/images/erd.png" alt="" />
