<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
    return view('layouts.master');
    });
    //CRUD kategori
    Route::get('/kategori','KategoriController@index');
    Route::get('/kategori/create', 'KategoriController@create');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori/{ecommerce_id}', 'KategoriController@show');
    Route::get('/kategori/{ecommerce_id}/edit', 'KategoriController@edit');
    Route::put('/kategori/{ecommerce_id}', 'KategoriController@update');
    Route::delete('/kategori/{ecommerce_id}', 'KategoriController@destroy');
    
    //Laravel Auth
    Route::get('/home', 'HomeController@index')->name('home');

    //CRUD
    Route::resource('account','ProfilController')->only([
        'index','update'
    ]);

    //CART
    
});

Auth::routes();
//CRUD Produk
    Route::resource('product','ProdukController');


