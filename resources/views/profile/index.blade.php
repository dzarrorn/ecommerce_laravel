@extends('layouts.master')
@section('content')
    <form action="/account/{{$profil->id}}" method="POST">
        @csrf
        @method('put')
        
        {{-- date of birth --}}
        <div class="col-md-6 mb-3">
            <h1>{{$profil->user->name}} Account</h1>
            <label for="tgl_lahir" >{{ __('Date of Birth') }}</label>
            <input type="date" name="tgl_lahir" class="form-control"  value="{{$profil->tgl_lahir}}" required>
            @error('tgl_lahir')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
        {{-- place of birth --}}
        <div class="col-md-6 mb-3">
            <label for="tmpt_lahir" >{{ __('Place of Birth') }}</label>
            <input type="text" name="tmpt_lahir" class="form-control"  value="{{$profil->tmpt_lahir}}" required>
            @error('tmpt_lahir')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
        {{-- Alamat --}}
        <div class="col-md-6 mb-3">
            <label for="alamat" >{{ __('Place of Birth') }}</label>
            <input type="text" name="alamat" class="form-control"  value="{{$profil->alamat->alamat}}" required>
            @error('alamat')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
        {{-- kota/kab --}}
        <?php
        $kota='kota/kab';
        ?>
        <div class="col-md-6 mb-3">
            <label for="kota/kab" >{{ __('City') }}</label>
            <input type="text" name="kota/kab" class="form-control"  value="{{$profil->alamat->$kota}}" required>
            @error('kota/kab')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
        {{-- kecamatan --}}
        <div class="col-md-6 mb-3">
            <label for="kecamatan" >{{ __('District') }}</label>
            <input type="text" name="kecamatan" class="form-control"  value="{{$profil->alamat->kecamatan}}" required>
            @error('kecamatan')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
        {{-- Kode Pos --}}
        <div class="col-md-6 mb-3">
            <label for="kode_pos" >{{ __('Code Pos') }}</label>
            <input type="text" name="kode_pos" class="form-control"  value="{{$profil->alamat->kode_pos}}" required>
            @error('kode_pos')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary ml-3">Update</button>
    </form>
@endsection