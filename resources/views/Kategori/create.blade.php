@extends('layouts.master')

@section('judul')
    Tambah Data Kategori
@endsection

@section('content')

        <form action="/kategori" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Kategori</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection
