@extends('layouts.master')

@section('judul')
    Edit Data Kategori {{$kategori->nama}}
@endsection

@section('content')

        <form action="/kategori/{{$kategori->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Kategori</label>
                <input type="text" class="form-control" value="{{$kategori->nama}}" name="nama" id="title" placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
@endsection
