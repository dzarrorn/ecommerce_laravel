@extends('layouts.master')
@section('content')
    <div class="single-products-catagory clearfix">
        <a href="shop.html">
            <img src="{{asset('template/img/bg-img/5.jpg')}}" alt="">
            <!-- Hover Content -->
            <div class="hover-content">
                <div class="line"></div>
                <p>From $18</p>
                <h4>Plant Pot</h4>
            </div>
        </a>        
    </div>
@endsection