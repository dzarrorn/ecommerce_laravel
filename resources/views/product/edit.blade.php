@extends('layouts.master')
@section('content')
    <h1>Edit Your Product</h1>
    <form action="/product/{{$product->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Name</label>
                <input type="text" class="form-control" value="{{$product->nama}}" name="nama" id="title" >
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Price</label>
                <input type="text" class="form-control" value="{{$product->harga}}" name="harga" id="body" >
                @error('harga')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Stock</label>
                <input type="text" class="form-control" value="{{$product->stok}}" name="stok" id="title" >
                @error('stok')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Image Product</label>
                <input type="file" class="form-control" value="{{$product->gambar}}" name="gambar">
                @error('gambar')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Category</label>
                <br>
                <select name="kategori_id" id="" class="form-control">
                    <option value="">-Select Category-</option>
                    @foreach ($kategori as $item)
                    @if ($item->id===$product->kategori_id)
                        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endif
                    @endforeach
                </select>
                @error('kategori_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <br><br>
            
            <div class="form-group">
                <label for="title">Description</label>
                <textarea name="deskripsi" id=""  class="form-control">{{$product->deskripsi}}</textarea>
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
@endsection