@extends('layouts.master')
@section('content')
    <h1>Add Your Product</h1>
    <form action="/product" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Name</label>
                <input type="text" class="form-control" name="nama" id="title" >
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Price</label>
                <input type="text" class="form-control" name="harga" id="body" >
                @error('harga')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Stock</label>
                <input type="text" class="form-control" name="stok" id="title" >
                @error('stok')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Image Product</label>
                <input type="file" class="form-control" name="gambar">
                @error('gambar')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            {{-- <div class="form-group">
                <label for="title">Category</label>
                <br>
                <select name="kategori_id" id="" class="form-control">
                    <option value="">-Select Category-</option>
                    @foreach ($kategori as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
                @error('kategori_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div> --}}
            <br><br>
            
            <div class="form-group">
                <label for="title">Description</label>
                <textarea name="deskripsi" id="" class="form-control"></textarea>
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
@endsection
