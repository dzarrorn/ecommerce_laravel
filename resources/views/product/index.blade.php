@extends('layouts.master')
@section('content')
@foreach ($product as $item)
    <div class="single-products-catagory clearfix">
        <a href="/product/{{$item->id}}">
            <img src="{{asset('gambar/'.$item->gambar)}}" alt="">
            <!-- Hover Content -->
            <div class="hover-content">
                <div class="line"></div>
                <p>From ${{$item->harga}}</p>
                <h4>{{$item->nama}}</h4>
            </div>
        </a>        
    </div>
@endforeach
    
@endsection


