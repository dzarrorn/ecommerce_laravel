@extends('layouts.master')
@section('content')
<div class="container mt-4"></div>
    <div class="row">
        <div class="col-4">
            
        </div>
        <div class="col-8">
            <h3>Description:</h3>
            <p>{{$product->deskripsi}}</p>
            <h6>Stock:{{$product->stok}}</h6>
            
            <a href="/product" class="btn btn-outline-dark btn-sm">Back</a>
            <a href="/product/{{$product->id}}/edit" class="btn btn-outline-dark btn-sm">Edit</a>
            
            <form action="/product/{{$product->id}}" method="POST">
                @method('delete')
                @csrf
                <input type="submit" value="Delete" class="btn btn-outline-dark btn-sm my-2">
            </form>
        </div>
    </div>
    
    
    <div class="single-products-catagory clearfix">
        <a href="#">
            <img src="{{asset('gambar/'.$product->gambar)}}" alt="">
            <!-- Hover Content -->
            <div class="hover-content">
                <div class="line"></div>
                <p>From ${{$product->harga}}</p>
                <h4>{{$product->nama}}</h4>
            </div>
        </a> 
    </div>
    

@endsection