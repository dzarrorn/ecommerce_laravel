@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach ($products as $product)
        <div class="col-md-3 mb-2">
            <div class="card">
                <img src="{{ url('gambar')}}/{{ $product->gambar}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">{{ $product->nama}}</h5>
                  <p class="card-text">
                      <strong>Price :</strong> $ {{($product->harga)}} <br>
                      <strong>Stock :</strong> {{($product->stok)}} <br>
                      <strong>Deskription :</strong> {{($product->deskripsi)}}
                  </p>
                  <a href="#" class="btn btn-primary">
                      <i class="fa fa-shopping-cart" aria-hidden="true"></i> Add cart</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
